--Copyright (C) 2010 <SWGEmu>


--This File is part of Core3.

--This program is free software; you can redistribute 
--it and/or modify it under the terms of the GNU Lesser 
--General Public License as published by the Free Software
--Foundation; either version 2 of the License, 
--or (at your option) any later version.

--This program is distributed in the hope that it will be useful, 
--but WITHOUT ANY WARRANTY; without even the implied warranty of 
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
--See the GNU Lesser General Public License for
--more details.

--You should have received a copy of the GNU Lesser General 
--Public License along with this program; if not, write to
--the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

--Linking Engine3 statically or dynamically with other modules 
--is making a combined work based on Engine3. 
--Thus, the terms and conditions of the GNU Lesser General Public License 
--cover the whole combination.

--In addition, as a special exception, the copyright holders of Engine3 
--give you permission to combine Engine3 program with free software 
--programs or libraries that are released under the GNU LGPL and with 
--code included in the standard release of Core3 under the GNU LGPL 
--license (or modified versions of such code, with unchanged license). 
--You may copy and distribute such a system following the terms of the 
--GNU LGPL for Engine3 and the licenses of the other code concerned, 
--provided that you include the source code of that other code when 
--and as the GNU LGPL requires distribution of source code.

--Note that people who make modified versions of Engine3 are not obligated 
--to grant this special exception for their modified versions; 
--it is their choice whether to do so. The GNU Lesser General Public License 
--gives permission to release a modified version without this exception; 
--this exception also makes it possible to release a modified version 


object_ship_tieaggressor = object_ship_shared_tieaggressor:new {

	name = "tieaggressor",
	slideFactor = 1.78,
	chassisHitpoints = 322,
	chassisMass = 10000,
	reactor = { name = "rct_generic", hitpoints = 98.6213, armor = 98.12754,},
	engine = { name = "eng_qualdex_kyromaster", hitpoints = 99.5431, armor = 95.85328, speed = 44.17288, pitch = 18.17101, roll = 18.29822, yaw = 17.39229, acceleration = 12.19066, rollRate = 40.52002, pitchRate = 41.43419, deceleration = 6.061667, yawRate = 41.94518,},
	shield_0 = { name = "shd_generic", hitpoints = 199.21, armor = 192.109, regen = 4.829192, front = 489.0012, back = 788.4413,},
	armor_0 = { name = "arm_generic", hitpoints = 598.0481, armor = 584.3405,},
	armor_1 = { name = "arm_generic", hitpoints = 570.3751, armor = 573.3532,},
	capacitor = { name = "cap_generic", hitpoints = 0, armor = 0, rechargeRate = 58.62266, energy = 1409.185,},
	weapon_0 = { name = "wpn_generic", hitpoints = 199.8276, armor = 192.0476, rate = 0.556226, drain = 27.39345, maxDamage = 358.1665, shieldEfficiency = 0, minDamage = 230.7688, ammo = 0, ammo_type = 0, armorEfficiency = 0,},
	weapon_1 = { name = "wpn_sienar_heatseeker_missile_s01", hitpoints = 192.4738, armor = 196.4408, rate = 3.82126, drain = 27.09309, maxDamage = 27.13664, shieldEfficiency = 0, minDamage = 26.93987, ammo = 50, ammo_type = 3, armorEfficiency = 0,},
}

ObjectTemplates:addTemplate(object_ship_tieaggressor, "object/ship/tieaggressor.iff")
